var Router = ReactRouter;

var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var NotFoundRoute = Router.NotFoundRoute;

var App = React.createClass({
	render: function () {
		return (
			<div>
				{/*This is app.*/}
				<RouteHandler/>
			</div>
		);
	}
});

var ArticlesBox = React.createClass({

	getInitialState: function() {
		return {articles: []};
	},

	loadArticlesFromServer: function() {
		var xmlhttp = new XMLHttpRequest();
		var outerthis = this;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var articles = JSON.parse(xmlhttp.responseText);
				outerthis.setState({articles: articles});
			}
		}
		xmlhttp.open("GET", "/articles/", true);
		xmlhttp.send();
	},
	
	componentDidMount: function() {
		this.loadArticlesFromServer();
	},
	
	render: function() {
		var articlesList = this.state.articles.map(function (article) {
			return (
				<div>
					<Link to="article" params={{articleId: article.id}}> 
						{article.title}
					</Link>
				</div>
			);
		});
		return (
			<div className="container-fluid"> 
				<h2>Articles list:</h2>
				{articlesList} 
			</div>
		);
	}
	
});

var Article = React.createClass({

	mixins: [Router.State],

	getInitialState: function() {
		return {comments: []};
	},
	
	loadArticleFromServer: function(id) {
		var xmlhttp = new XMLHttpRequest();
		var outerthis = this;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var article = JSON.parse(xmlhttp.responseText);
				outerthis.setState({article: article});
			}
		}
		xmlhttp.open("GET", "/articles/" + id + "/", true);
		xmlhttp.send();
	},
	
	loadCommentsFromServer: function(id) {
		var xmlhttp = new XMLHttpRequest();
		var outerthis = this;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var comments = JSON.parse(xmlhttp.responseText);
				outerthis.setState({comments: comments});
			}
		}
		xmlhttp.open("GET", "/articles/" + id + "/comments/", true);
		xmlhttp.send();
	},
	
	loadCommentToServer: function(comment) {
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", "/comment/");
		xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xmlhttp.send(JSON.stringify(comment));
	},
	
	componentDidMount: function() {
		this.loadArticleFromServer(this.getParams().articleId);
		this.loadCommentsFromServer(this.getParams().articleId);
	},
	
	handleCommentSubmit: function(comment) {
		var newComment = {article: this.getParams().articleId, comment_author: comment.author, comment_text: comment.text};
		var comments = this.state.comments;
		var newComments = comments.concat([newComment]);
		this.setState({comments: newComments});
		this.loadCommentToServer(newComment);
	},
	
	render: function() {
		if (!this.state.article) {
			return (
				<div className="container">
					<Link to="app" className="btn btn-info">Back to list</Link>
				</div>
			);
		}
		return (
			<div className="container">
				<Link to="app" className="btn btn-info">Back to list</Link>
				<br/>
				<div className="jumbotron">
					<h2>{this.state.article.title}</h2>
					<h3><small>Author: </small>{this.state.article.article_author}</h3>
					<p>{this.state.article.article_text}</p>
				</div>
				<h3>Comments:</h3>
				<CommentList data={this.state.comments} />
				<CommentForm onCommentSubmit={this.handleCommentSubmit} />
			</div>
		);
	}
});

var CommentForm = React.createClass({
	handleSubmit: function(e) {
		e.preventDefault();
		var author = this.refs.author.getDOMNode().value.trim();
		var text = this.refs.text.getDOMNode().value.trim();
		if (!text || !author) {
		  return;
		}
		this.props.onCommentSubmit({author: author, text: text});
		this.refs.author.getDOMNode().value = '';
		this.refs.text.getDOMNode().value = '';
	},

	render: function() {
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="form-group">
					<label>Name:</label>
					<input type="text" className="form-control" placeholder="Your name" ref="author" />
				</div>
				<div className="form-group">
					<label>Comment text:</label>
					<textarea className="form-control" ref="text"></textarea>
				</div>
				<input type="submit" className="btn btn-default" value="Post" />
			</form>
		);
	}
});

var CommentList = React.createClass({
	render: function() {
		var commentNodes = this.props.data.map(function (comment) {
			return (
				<Comment author={comment.comment_author}>
					{comment.comment_text}
				</Comment>
			);
		});
		return (
			<ul className="list-group">
				{commentNodes}
			</ul>
		);
	}
});

var Comment = React.createClass({
	render: function() {
		return (
			<li className="list-group-item">
				<h4>{this.props.author}</h4>
				<p>{this.props.children}</p>
			</li>
		);
	}
});

var NotFound = React.createClass({
	render: function() {
		return (
			<div>
				Error 404. Page not found.
			</div>
		);
	}
});


var routes = (
	<Route name="app" path="/" handler={App}>
		<Route name="article" path="article/:articleId" handler={Article} />
		<DefaultRoute handler={ArticlesBox} />
		<NotFoundRoute handler={NotFound}/>
	</Route>
	
);

Router.run(routes, Router.HistoryLocation, function (Handler) {
  React.render(<Handler/>, document.getElementById("content"));
});