var ArticlesBox = React.createClass({

	getInitialState: function() {
		return {articles: [], article_id: -1};
	},

	loadArticlesFromServer: function() {
		var xmlhttp = new XMLHttpRequest();
		var outerthis = this;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var articles = JSON.parse(xmlhttp.responseText);
				outerthis.setState({articles: articles});
			}
		}
		xmlhttp.open("GET", "/articles/", true);
		xmlhttp.send();
	},
	
	componentDidMount: function() {
		this.loadArticlesFromServer();
	},
	
	handleShowArticleDetail: function(article) {
		this.setState({article_id: article.id});
	},
	
	render: function() {
		if (this.state.article_id > 0) {
			return (
				<Article id={this.state.article_id} />
			);
		}
		var outerthis = this;
		var articlesList = this.state.articles.map(function (article) {
			return (
				<ArticleLink id={article.id} onShowArticleDetail={outerthis.handleShowArticleDetail}> 
					{article.title} 
				</ArticleLink>
			);
		});
		return (
			<div> 
				{articlesList} 
			</div>
		);
	}
});

var ArticleLink = React.createClass({
	
	handleSubmit: function(e) {
		e.preventDefault();
		var id = this.refs.id.getDOMNode().value.trim();
		this.props.onShowArticleDetail({id:id});
	},

	render: function() {
		return (
			<div>
				<h3>{this.props.children}
				<form className="commentForm" onSubmit={this.handleSubmit}>
					<input type="hidden" ref="id" value={this.props.id} />
					<input type="submit" value="read" />
				</form>
				</h3>
			</div>
		);
	}
});

var Article = React.createClass({
	getInitialState: function() {
		return {comments: []};
	},
	
	loadArticleFromServer: function(id) {
		var xmlhttp = new XMLHttpRequest();
		var outerthis = this;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var article = JSON.parse(xmlhttp.responseText);
				outerthis.setState({article: article});
			}
		}
		xmlhttp.open("GET", "/articles/" + id + "/", true);
		xmlhttp.send();
	},
	
	loadCommentsFromServer: function(id) {
		var xmlhttp = new XMLHttpRequest();
		var outerthis = this;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var comments = JSON.parse(xmlhttp.responseText);
				outerthis.setState({comments: comments});
			}
		}
		xmlhttp.open("GET", "/articles/" + id + "/comments/", true);
		xmlhttp.send();
	},
	
	loadCommentToServer: function(comment) {
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("POST", "/comment/");
		xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xmlhttp.send(JSON.stringify(comment));
	},
	
	componentDidMount: function() {
		this.loadArticleFromServer(this.props.id);
		this.loadCommentsFromServer(this.props.id);
	},
	
	handleCommentSubmit: function(comment) {
		var newComment = {article: this.props.id, comment_author: comment.author, comment_text: comment.text};
		var comments = this.state.comments;
		var newComments = comments.concat([newComment]);
		this.setState({comments: newComments});
		this.loadCommentToServer(newComment);
	},
	
	render: function() {
		if (!this.state.article) return (<div></div>);
		return (
			<div>
				<h2>{this.state.article.title}</h2>
				<h3>{this.state.article.article_author}</h3>
				<p>{this.state.article.article_text}</p>
				<h2>Comments:</h2>
				<CommentList data={this.state.comments} />
				<CommentForm onCommentSubmit={this.handleCommentSubmit} />
			</div>
		);
	}
});

var CommentForm = React.createClass({
	handleSubmit: function(e) {
		e.preventDefault();
		var author = this.refs.author.getDOMNode().value.trim();
		var text = this.refs.text.getDOMNode().value.trim();
		if (!text || !author) {
		  return;
		}
		this.props.onCommentSubmit({author: author, text: text});
		this.refs.author.getDOMNode().value = '';
		this.refs.text.getDOMNode().value = '';
	},

	render: function() {
		return (
			<form className="commentForm" onSubmit={this.handleSubmit}>
				<input type="text" placeholder="Your name" ref="author" />
				<input type="text" placeholder="Say something..." ref="text" />
				<input type="submit" value="Post" />
			</form>
		);
	}
});

var CommentList = React.createClass({
	render: function() {
		var commentNodes = this.props.data.map(function (comment) {
			return (
				<Comment author={comment.comment_author}>
					{comment.comment_text}
				</Comment>
			);
		});
		return (
			<div className="commentList">
				{commentNodes}
			</div>
		);
	}
});

var Comment = React.createClass({
	render: function() {
		return (
			<div className="comment">
				<h3 className="commentAuthor">
					{this.props.author}
				</h3>
				<p>{this.props.children}</p>
			</div>
		);
	}
});

React.render(<ArticlesBox />, 
	document.getElementById("content")
);