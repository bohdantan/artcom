from django.contrib import admin
from articles.models import Article, Comment

class CommentInline(admin.TabularInline):
    model = Comment
    extra = 1

class ArticleAdmin(admin.ModelAdmin):
    fields = ['title', 'article_author', 'article_text']
    inlines = [CommentInline]

admin.site.register(Article, ArticleAdmin)
