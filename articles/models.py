from django.db import models

class Article(models.Model):
    title = models.CharField(max_length=100)
    article_author = models.CharField(max_length=100)
    article_text = models.TextField()

    def __unicode__(self):
        return self.title

class Comment(models.Model):
    article = models.ForeignKey(Article)
    comment_author = models.CharField(max_length=100)
    comment_text = models.TextField()
    
