from django.conf.urls import patterns, url

from articles import views

urlpatterns = patterns('',
    url(r'^articles/$', views.article_list),
    url(r'^articles/(?P<pk>[0-9]+)/$', views.article_detail),
    url(r'^articles/(?P<pk>[0-9]+)/comments/$', views.article_comments),
    url(r'^comment/$', views.comment),
    url(r'.*', views.index, name='index'),
)
