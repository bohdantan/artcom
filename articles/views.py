from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from articles.models import Article, Comment
from articles.serializers import ArticleSerializer, CommentSerializer, ArticleIdTitleSerializer

class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

def article_list(request):
    if request.method == 'GET':
        articles = Article.objects.all()
        serializer = ArticleIdTitleSerializer(articles, many=True)
        return JSONResponse(serializer.data)

def article_detail(request, pk):
    try:
        article = Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return HttpResponse(status=404)
    
    if request.method == "GET":
        serializer = ArticleSerializer(article)
        return JSONResponse(serializer.data)

def article_comments(request, pk):
    try:
        article = Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == "GET":
        serializer = CommentSerializer(article.comment_set.all(), many=True)
        return JSONResponse(serializer.data)

@csrf_exempt
def comment(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        serializer = CommentSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)

def index(request):
    return render(request, 'articles/index.html')
    
